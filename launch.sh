#!/bin/bash

. /etc/sysconfig/openafs
echo cern.ch > /usr/vice/etc/ThisCell
echo cern.ch > /etc/openafs/ThisCell
cp -a /host/lib/modules/$(uname -r) /lib/modules/
dnf install -y /root/kernel-devel-*.rpm
dkms build -m openafs -v 1.8.6-1.fc31
dkms install -m openafs -v 1.8.6-1.fc31
depmod -a
modprobe openafs
/usr/sbin/afsd $AFSD_ARGS
/usr/bin/afs-setserverprefs

while [[ $(lsmod | grep openafs) ]]; do
	sleep 60
done
