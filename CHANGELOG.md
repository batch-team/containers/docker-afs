# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Removed

## [31.0] - 2020-11-23
### Added

- 'clean all' statements to reduce image size.
- `xz` rpm required. Without it, the container would fail to build the module at startup.

### Changed

- Bump base version and related repos to 31

### Removed

## [29.0] - 2019-08-14

### Added

- Initial Dockerfile for Fedora 29
