FROM fedora:31

LABEL maintainer="ben.dylan.jones@cern.ch, batch-operations@cern.ch"

RUN dnf install -y dnf-plugins-core && dnf clean all
RUN dnf -y copr enable lfernand/openafs
RUN dnf update -y && dnf clean all
RUN dnf install -y krb5-workstation xz\
		openafs-krb5 openafs-client dkms-openafs \
		perl-Getopt-Long perl-Sys-Syslog iproute && \
		dnf clean all

ADD sysconfig/openafs /etc/sysconfig/openafs
ADD CellServDB /usr/vice/etc/
ADD CellServDB.dist /usr/vice/etc/
ADD CellServDB.local /usr/vice/etc/
ADD CellServDB /etc/openafs/
ADD CellServDB.dist /etc/openafs/
ADD CellServDB.local /etc/openafs/
ADD krb5.conf /etc/

ADD kernel-devel-5.4.8-200.fc31.x86_64.rpm /root/

RUN /bin/chmod 0644 /usr/vice/etc/CellServDB
RUN /bin/chmod 0644 /usr/vice/etc/CellServDB.dist
RUN /bin/chmod 0644 /usr/vice/etc/CellServDB.local
RUN /bin/chmod 0644 /etc/openafs/CellServDB
RUN /bin/chmod 0644 /etc/openafs/CellServDB.dist
RUN /bin/chmod 0644 /etc/openafs/CellServDB.local
RUN /bin/mkdir -p /afs

ADD afs-setserverprefs /usr/bin/
ADD launch.sh /launch.sh

ENTRYPOINT ["/launch.sh"]
