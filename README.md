# Docker AFS for CERN Kubernetes clusters

This Docker image is designed to be used by our Kubernetes clusters to provide access to AFS.

## Releases

There is an image release matching the minion's version provided by OpenStack Magnum.

Releasing a new image implies:

* Checking the target cluster version of the kernel and Fedora running.
* Check if our current openafs builds provider `copr/jsbillings` has already built `openafs` for our Fedora version. If not, you can try to build it yourself in copr.
* Update the `Dockerfile` pointing to download the `kernel-devel` RPM matching the minions' kernel version.

## Details

TODO